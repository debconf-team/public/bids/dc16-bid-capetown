# Clare Bock, Apetite Catering

* Lunch R80 pppd
* Dinners R150.00 pppd
* DELIVERY & SET UP R300.00 per day
* STAFF for 50 People R5100.00 per day
* R3450.00 – this price includes the delivery but excludes any breakages and shortages that may occur – this is for 1 or 2 days hire
* The hiring company will charge approx R7000.00 for 5 days hire
* ALL THE ABOVE PRICES EXCLUDE VAT
